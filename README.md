# Creative Script

## Introduction

The Creative Script provides a quick and easy guideline and API method for the 3rd Party Rich Media Production to fulfill Hotmob ADs' In-App Container.

## Features

- An easy guideline for integration with Custom Make HTML5.
- An easy guideline for integration with Google Web Designer.

## Material Requirement
#### Image Material Specifications

To leverage the display quality of devices with higher pixel density (such as Apple’s Retina displays) in your creatives, your image assets (jpg, gif, png) must be prepared in twice the height and width as you would need them to appear.
(To mitigate the impact of using larger images on page weight, recommend using a heavy compression technique on your images.)

|                  | Lower Performance Example (1x):          | Best Performance Example (2x or above):  |
| ---------------- | ---------------------------------------- | ---------------------------------------- |
| Image Dimensions | 320 x 460 (Android APPs)<br />320 x 548 (iPhone APPS)<br />320 x 460 (Mobile Web) | 640 x 920 (Android APPs)<br />640 x 1096 (Iphone APPs)<br />640 x 920 (Mobile Web) |
| Description      | If using Lower Resolution image, the ads image will be blurred | If using High Resolution Image, the ads should be scale to fit the device |

  

#### HTML5 Specifications
Must set to 320px for `<body>` tags on all platform. Width of `<div>` cannot exceed 320px. If you need the ads show as High Resolution, you have to ensure the ads are using 2x or above image material(jpg, png, gif, Not include HTML5)


- Total file size (Including HTML5, image, css, js files): not exceed 1MB
- Support CSS, JavaScript
- On Mobile Apps, clients must handle the Vendor-Prefix CSS3 and JavaScript for iOS uiWebView and Android Webview.
- On Mobile Web, clients should handled CSS3 and JavaScript to support iOS Safari and Android Browser.
- Width must set to 320px for `<body>` tags on all platform. Width of `<div>` cannot exceed 320px.
  The maximum height on iPhone4 / Android / Mobile Web is 460px and maximum height on iPhone5 is 548px.
- Transparent background & form submit are not allowed
- Content preload is required
- Please use [setClickThrough()](doc/api-methods/README.md#cmsetclickthrough) for URL redirection
- Please avoid important message or logo in the top right corner, as there is a default cross icon by the app. (Do not create any cross button)

<center><img src="./images/proper_creative_cross.jpg"></center>
  
|  Platform    | HTML5 Dimensions | Cross Area    |
| ------------:| ---------------- | ------------- |
| Android APPs | 320 x 460px      | 48px x 48px   |
| iPhone APPs  | 320 x 548px      | 48px x 48px   |
| Mobile Web   | 320 x 460px      | 48px x 48px   |{: style="width:100%"}

**Overall, you need to submit:**
1.  HTML5 Interstitial URL per platform & per resolution (hosted) 
2.  Landing Page URL

## Next Step
* Go to [How to Build a Hotmob Compatible GWD Creative](doc/hotmob-compatible-gwd-creative/README.md) if you are using **Google Web Designer(GWD)** to build your creative
* Go to [Basic Implementation](doc/basic-implementation/README.md) if you are using **other methods** to build your creative.
* Go to [API Methods](doc/api-methods/README.md) if you have to using addition features to build your creative.
* Go to [Self-test Before Submitting Materials](doc/test-case/README.md) if you have been finished your creative.

## Where do I report issues?
Please report issues on the [issues page](https://gitlab.com/hotmob-creative/creative-sdk-support/issues).
