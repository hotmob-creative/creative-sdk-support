# Summary
* [Table of Content](README.md)
* [Material Requirement](hotmob-creative-script/requirement/README.md)
* Getting Started
  * [Basic Implementation](hotmob-creative-script/basic-implementation/README.md)
  * [Hotmob Compatible GWD Creative](hotmob-creative-script/hotmob-compatible-gwd-creative/README.md)
* [API Methods](hotmob-creative-script/api-methods/README.md)
* [Self-test Before Submitting Materials](hotmob-creative-script/test-case/README.md)

