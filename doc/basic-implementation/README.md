## Basic Implementation

1.    After creating your creative (if you use **Google Web Designer(GWD)**, please refer the Section - [**How to Build a Hotmob Compatible GWD Creative**](../hotmob-compatible-gwd-creative/README.md)), open the HTML file of your creative.

2.    Embed the Hotmob creative.sdk.js in the HTML header.

      ```
      <script type="text/javascript" src="https://code.hot-mob.com/api/javascript/creative/creative.sdk.min.js"></script>
      ```

3.    Copy the code below. There are different handling methods for **responsive** and **non-responsive** creative.

      **For Responsive Creative:**

      ```
          <script>
            var cm = new hmCreativeManager();
            cm.init({
             defaultCSS: true
            });
          </script>
      ```

      **For Non-Responsive Creative:**

              <script>
                var cm = new hmCreativeManager();
                cm.init({
                  defaultCSS: true,
                  defaultViewport: true,
                  defaultDimensions: true,
                });
              </script>

4.    Paste the code copied just before the  ```</body> ``` tag in your creative HTML.
  After the basic implementation of Hotmob Creative SDK,  the creative should scale properly in the common devices.

<center><img src="./images/proper_creative.jpg" width="400px"></center>

<center><strong>A 320px(width) x 548px(height) creative on iphone 7 Plus after the implementation </strong></center>




