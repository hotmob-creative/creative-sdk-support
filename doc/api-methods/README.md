## API Methods

#### cm.init()
##### Description
This is the init function of the Hotmob Creative SDK. You need to trigger this function before calling any others API methods. All options are optional. If option value is not entered by users, default value will be applied (if any).   

##### Syntax
```javascript
        var cm = new hmCreativeManager();
        cm.init({
          /*Options configuration*/
          defaultCSS: true
        });
```
##### Parameters
| Name    | Data Type | Description                              |
| ------- | --------- | ---------------------------------------- |
| Options | Object    | An object that contains the option parameters |

##### Options Parameters
| Name              | Data Type | Description                              |
| ----------------- | --------- | ---------------------------------------- |
| defaultCSS        | boolean   | determines whether the creative applies Hotmob-favoured CSS.<br /><br />**Hotmob-favoured CSS**:<br />background color:black;<br />overflow:hidden;<br />position of the creative: center of the screen |
| defaultViewport   | boolean   | determines whether the creative applies Hotmob-favoured viewport.<br /><br />**Hotmob-favoured Viewport:**<br />user-scalable: 0;<br />*(for **non-responsive** creative)*<br />width: 320;<br /> |
| defaultDimensions | boolean   | determines whether the creative applies dimensions from Hotmob. |


#### cm.getClickThrough()
##### Description
This function returns the landing page redirect URL of the creative from Hotmob.  The resulting URL will indicate if it is open with in-app browser or external browser.

##### Syntax
```javascript
        document.getElementById("link").href = cm.getClickThrough();
```

##### Parameters
No parameter


#### cm.setClickThrough()
##### Description
This function is used to set the redirect URL to a specific area (e.g  body of the HTML). If user wants the whole creative to be clickable to landing page. Please input "body" for clickArea.

##### Syntax
```javascript
        cm.setClickThrough("toLanding","body", cm.getClickThrough());
```

##### Parameters
| Name      | Data Type | Description                              |
| --------- | --------- | ---------------------------------------- |
| id        | string    | Id for the click link                    |
| container | string    | The clickable area for the redirect link |
| actionURL | string    | The redirect URL for the destination     |
| zIndex    | number    | The z-index for the clickable area       |


#### cm.getDimensions()
##### Description
This function returns the dimension object of the creative from Hotmob.
##### Syntax
```javascript
    	var dimensions = cm.getDimensions();
		var width = dimensions.width;
		var height = dimensions.height;
```
##### Parameters
No parameter
##### Returns Response
Returns object with width and height properties. 

| Name   | Type   | Description      |
| ------ | ------ | ---------------- |
| width  | number | Width dimension  |
| height | number | Height dimension |


#### cm.getUserAgent():
##### Description
This function returns the user device type. 
##### Syntax
```javascript
        var userDevice = cm.getUserAgent();
```
##### Parameters
No parameter
