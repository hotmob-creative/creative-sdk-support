## How to Build a Hotmob Compatible Google Web Designer (GWD) Creative 

### Step 1 - Create your creative
To create a new GWD creative,

1. set the type to “**interstitial**” environment to “**Non - Google ad**”. 
2. It is suggested to tick the check-box – “**Responsive Layout**” as well to enhance device compatibility.

![img](./images/check_responsive_layout.jpg)

If  “**Responsive Layout**” is checked, the container of the creative will take 100% width and 100% height of the device. It is suggested to change every element to use percentage for their size and position if possible.

![img](./images/responsive_layout.jpg)

If “**Responsive Layout**” is not checked, the creative is in fixed size. i.e.: if the dimensions of the creative is set as 320px width and 548px height. The output file will be a fixed size (320px x 548px) HTML. With the default viewport setting of GWD, the creative will not be displayed properly. 

<center><img src="./images/improper_creative.jpg" width="400px"></center>

<center><strong>A 320px(width) x 548px(height) creative on iphone 7 Plus<br>with the default viewport setting of GWD</strong></center>



### Step 2 - Deploy and Initialize Hotmob Creative SDK

1. Go to your Code view in GWD

<img src="./images/11.jpg" >

2. To deploy Hotmob Creative SDK, paste the Hotmob Creative SDK below before the closing tag of the head - ```</head>```
```
<script type="text/javascript" src="https://code.hot-mob.com/api/javascript/creative/creative.sdk.min.js"></script>
```
<img src="./images/12.jpg">


3. To initialize Hotmob Creative SDK, Paste the code below inside the function **handleDomContentLoaded**.  For configuration, please refer the section - [Options](../api-methods/README.md#options-parameters) of [Init Function](../api-methods/README.md#cminit)


**For "Responsive Layout" Creative:**

```javascript
      var cm = new hmCreativeManager();
      cm.init({
        defaultCSS: true
      });
```


**For non "Responsive Layout" Creative:**

```javascript
      var cm = new hmCreativeManager();
      cm.init({
        defaultCSS: true,
        defaultViewport: true,
        defaultDimensions: true,
      });
```
<center><img src="./images/gwd_editor_deploy.jpg"></center>

After the deployment of Hotmob SDK, the layout issue of non-responsive creative mentioned in Step 1 must be fixed.



### Step 3 - Add Redirect URL to your creative

Use the [setClickthrough()](../api-methods/README.md#cmsetclickthrough) function to add the redirect URL
<center><img src="./images/setClickThrough.jpg"></center>

If you want to delay the attachment of the redirect URL, use the **javascript setTimeout() function**:

```javascript
setTimeout(function(){
        cm.setClickThrough({
          id:"toLanding",
          container:"body",
          zIndex:999,
          href:cm.getClickThrough()
        })	
},10000)
```

### Hotmob is not accepting any of creatives which have not follow [material requirements](https://gitlab.com/hotmob-creative/creative-sdk-support#material-requirement). Please check the [example](https://gitlab.com/hotmob-creative/creative-sdk-support/tree/master/example/gwd) for the different between accepted and not accepted creatives
