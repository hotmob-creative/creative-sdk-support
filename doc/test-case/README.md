## Self-test Before Submitting Materials

After implementation of Hotmob Creative SDK, you are expected to check the followings:

1. Open your HTML files with Chrome 

2. Press F12 to access the DevTools
   <center><img src="images/f12.jpg"></center>

3. Toggle on the **Device toolbar** to test your creative's device compatibility.
   <center><img src="images/toggle.jpg"></center>

4. Switch between different devices to check if the creative display properly.
   <center><img src="images/switch.jpg"></center>

5. If not, go to the **console** tab to see any related error(s). 
   <center><img src="images/console.jpg"></center>


